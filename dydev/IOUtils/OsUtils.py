'''
Created on 17.12.2012

@author: Stepan Dyatkovskiy, stpworld@narod.ru
'''
import os
from os import stat
from os import kill
from os import path
import signal
import platform
import re
from contextlib import contextmanager
import subprocess
from os import stat
from stat import ST_SIZE
from datetime import datetime

class Executor(object):
    def __init__(self, cmd, (stdOutFile, stdErrFile), logger=None):
        self.cmd = cmd
        self.stdOutFile = stdOutFile
        self.stdErrFile = stdErrFile
        self.logger = logger

    def __log(self, msg):
        if self.logger:
            self.logger.log(msg)

    def __cat(self, fileName):
        if self.logger:
            self.logger.cat(fileName)

    def run(self):
        pattern = re.compile(r'''((?:[^\s"']|"[^"]*"|'[^']*')+)''')
        args = [arg.replace('"', '').replace("'", "")
                for arg in pattern.split(self.cmd)[1::2]]

        with mktemps(count=2) as [tmpStdOut, tmpStdErr]:

            with open(tmpStdOut, 'wb') as stdOut:
                with open(tmpStdErr, 'wb') as stdErr:
                    self.__log("exec: %s\n" % str(args))
                    p = subprocess.Popen(args,
                                         stdout=stdOut,
                                         stderr=stdErr)
                    p.wait()

            if self.stdOutFile:
                with open(tmpStdOut, 'rb') as stdOut:
                    with open(self.stdOutFile, 'ab') as f:
                        f.write(stdOut.read())

            if self.stdErrFile:
                with open(tmpStdErr, 'rb') as stdErr:
                    with open(self.stdErrFile, 'ab') as f:
                        f.write(stdErr.read())

            self.__log("exec finished, stdout:\n")
            self.__cat(tmpStdOut)
            self.__log("\nstderr:\n")
            self.__cat(tmpStdErr)

        return p.returncode == 0

class OsUtils:

    def __init__(self, executorTy, logger):
        self.executorTy = executorTy
        self.logger = logger
        if logger:
            self.loggerExecutor = logger.down()
        else:
            self.loggerExecutor = None

    def __log(self, msg):
        if self.logger:
            self.logger.log(msg)
        else:
            print msg

    def __getProcessList(self, processMask):
        """
        Emits pairs collection, where key is process name,
        and value is pid
        """
        with mktemps(count=4) as [
            taskListOutput,
            taskListErr,
            taskListPSA,
            taskListPSAErr]:

            titleStrings = None

            if platform.system() == 'Windows':
                self.execute('tasklist /fi "IMAGENAME eq %s"' % processMask,
                             (taskListOutput, taskListErr),
                             doEchoCmd=False)
                titleStrings = 3
            else:
                self.execute('ps -A -o "%c %p"', (taskListPSA, taskListPSAErr),
                             doEchoCmd=False)
                processMask = processMask.replace('*', '.*')
                self.execute('grep -e "^%s" %s' % (processMask, taskListPSA),
                             (taskListOutput, taskListErr),
                             doEchoCmd=False)

                titleStrings = 0

            self.__log("Need to kill:")
            if self.logger:
                self.logger.cat(taskListOutput)

            with open(taskListOutput) as f:
                try:
                    lines = f.readlines()
                    if len(lines) > titleStrings:
                        for l in lines[titleStrings:]:
                            pattern = re.compile(r'''([^\s]+)''')
                            nonSpaces = pattern.split(l)[1::2]
                            yield (nonSpaces[0], nonSpaces[1])
                except IOError:
                    self.__log(
                        "Ooops. Can't read tasklist output." %
                        taskListOutput)
                    self.__logExecutionHigh("Tasklist stdErr:")
                    if self.logger:
                        self.logger.cat(taskListErr)

    def killall(self, processMask):
        procs = self.__getProcessList(processMask)
        for p in procs:
            pid = int(p[1])
            if platform.system() == 'Windows':
                cmd = 'taskkill /PID %d /T' % pid
                self.execute(cmd, (None, None), False)
            else:
                kill(pid, signal.SIGKILL)

    def remove(self, fileNames):
        try:
            if isinstance(fileNames, str):
                if fileExists(fileNames):
                        os.remove(fileNames)
            else:
                for fn in fileNames:
                    if fileExists(fn):
                        os.remove(fn)
        except:
            self.__log("Can't remove %s, sorry :-(" % fileNames)

    def execute(self, cmd, stdOutErr, doEchoCmd=True):
        self.__log("executing: %s" % cmd)
        if doEchoCmd:
            if stdOutErr[0]:
                self.echo("CMD: %s" % cmd, stdOutErr[0])
            if stdOutErr[1]:
                self.echo("CMD: %s" % cmd, stdOutErr[1])

        return self.executorTy(cmd, stdOutErr, self.loggerExecutor).run()

def touch(fileName):
    with open(fileName, 'w'): pass

def fileExists(fileName):
    return path.isfile(fileName)

def isEmpty(fileName):
    return stat(fileName)[ST_SIZE] == 0

def existsNotEmpty(fileNames, reportNonExisting=None):

    if isinstance(fileNames, str):
        return fileExists(fileNames) and not isEmpty(fileNames)

    for f in fileNames:
        if not fileExists(f) or isEmpty(f):
            return False if not reportNonExisting else (False, f)
    return True if not reportNonExisting else (True, None)


@contextmanager
def openFiles(fileNames):
    files = []
    try:
        files = [ open(fn) for fn in fileNames ]
        yield files
    finally:
        for f in files:
            if f:
                f.close()

def remove(fileNames):
    OsUtils(None, None).remove(fileNames)

@contextmanager
def mktemps(twine="tmp", count=1):
    try:
        temps = []
        tempFileName = twine
        suffix = None
        while len(temps) < count:
            while fileExists(tempFileName):
                if suffix is None:
                    suffix = 0
                else:
                    suffix += 1
                tempFileName = '%s%s' % (twine, suffix)
            temps.append(tempFileName)
            touch(tempFileName)
        yield temps
    finally:
        OsUtils(None, None).remove(temps)

class timediff:
    def __init__(self):
        self.start = None
        self.end = None
    def total_seconds(self):
        return (self.end - self.start).total_seconds()
    def __str__(self):
        UNIX_EPOCH = datetime.fromtimestamp(0)
        return "%s:%s" % \
            ((self.start - UNIX_EPOCH).total_seconds(),
             (self.end - UNIX_EPOCH).total_seconds())

    def fromstr(self, strValue):
        start, end = strValue.split(":")
        start, end = float(start), float(end)
        self.start = datetime.fromtimestamp(float(start))
        self.end = datetime.fromtimestamp(float(end))
        return self

@contextmanager
def measure(t):
    t.start = datetime.now()
    try:
        yield t
    finally:
        t.end = datetime.now()

def matches(strVal, regExp):
    pattern = re.compile(regExp)
    combs = pattern.split(strVal)
    return len(combs) > 1
