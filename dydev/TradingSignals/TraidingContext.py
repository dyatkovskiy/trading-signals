'''
Created on May 26, 2017

@author: stepan
'''
import sys

class TraidingContext(object):
    '''
    classdocs
    '''


    def __init__(self, pricesSource, priceProcessor):
        '''
        Constructor
        '''
        self.pricesSource = pricesSource
        self.pricesProcessor = priceProcessor

    def processPrices(self):
        self.pricesProcessor.process(self.pricesSource)

    def dumpPrices(self):
        self.pricesSource.dumpPricesTable(sys.stdout)