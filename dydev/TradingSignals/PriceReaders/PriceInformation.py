'''
Created on Apr 25, 2017

@author: stepan
'''
import datetime

ID_IDX = 0
DATE_IDX = ID_IDX + 1
TIME_IDX = DATE_IDX + 1
MIN_PRICE_IDX = TIME_IDX + 1
MAX_PRICE_IDX = MIN_PRICE_IDX + 1
OPEN_PRICE_IDX = MAX_PRICE_IDX + 1
CLOSE_PRICE_IDX = OPEN_PRICE_IDX + 1

def dbCol(colId):
    if colId is ID_IDX:
        return ID_IDX;
    return colId + 1

def csvCol(colId):
    return colId - 1

CSV_DATE_FORMAT = "%m/%d/%y"
CSV_TIME_FORMAT = "%H:%M.%S"

class PriceInformation(object):
    '''
    classdocs
    '''
    def __init__(self):
        self.datetime = None
        self.minPrice = 0
        self.maxPrice = 0
        self.openPrice = 0
        self.closePrice = 0
        self.date = None
        self.time = None

    def fromLivermorePrice(self, p):
        self.datetime = p.getDatetime()
        self.minPrice = p.getValue()
        self.maxPrice = p.getValue()
        self.openPrice = p.getValue()
        self.closePrice = p.getValue()
        self.date = self.datetime.date
        self.time = self.datetime.time
        return self

    def fromCSV(self, priceStrCols, \
                      dateFormat=CSV_DATE_FORMAT,
                      timeFormat=CSV_TIME_FORMAT,
                      dateColIdx=None, \
                      timeColIdx=None, \
                      openColIdx=None, \
                      highColIdx=None, \
                      lowColIdx=None, \
                      closeColIdx=None):
        self.id = None
        datetimeFormat = dateFormat + timeFormat
        strTime = priceStrCols[timeColIdx] if timeColIdx else "00:00.00"
        self.datetime = datetime.datetime.strptime(priceStrCols[dateColIdx] + strTime, datetimeFormat)

        self.date = self.datetime.date
        self.time = self.datetime.time

        self.minPrice = float(priceStrCols[lowColIdx])
        self.maxPrice = float(priceStrCols[highColIdx])
        self.openPrice = float(priceStrCols[openColIdx])
        self.closePrice = float(priceStrCols[closeColIdx])
        return self

    def fromDb(self, priceStrCols, dateFormat, timeFormat):

        self.id = priceStrCols[dbCol(ID_IDX)]

        datetimeFormat = "%s %s" % (dateFormat, timeFormat)
        datetimeValue = "%s %s" % (priceStrCols[dbCol(DATE_IDX)], priceStrCols[dbCol(TIME_IDX)])
        self.datetime = datetime.datetime.strptime(datetimeValue, datetimeFormat)

        self.date = self.datetime.date
        self.time = self.datetime.time

        self.minPrice = float(priceStrCols[dbCol(MIN_PRICE_IDX)])
        self.maxPrice = float(priceStrCols[dbCol(MAX_PRICE_IDX)])
        self.openPrice = float(priceStrCols[dbCol(OPEN_PRICE_IDX)])
        self.closePrice = float(priceStrCols[dbCol(CLOSE_PRICE_IDX)])
        return self

    def getDatetime(self):
        return self.datetime

    def getDate(self):
        return self.date

    def getTime(self):
        return self.time

    def getMax(self):
        return self.maxPrice

    def getMin(self):
        return self.minPrice

    def getOpen(self):
        return self.openPrice

    def getClose(self):
        return self.closePrice

    def __add__(self, other):
        res = PriceInformation()
        res.datetime = other.datetime
        res.date = other.date
        res.time = other.time
        res.minPrice = self.minPrice + other.minPrice
        res.maxPrice = self.maxPrice + other.maxPrice
        res.openPrice = self.openPrice + other.openPrice
        res.closePrice = self.closePrice + other.closePrice
        return res
