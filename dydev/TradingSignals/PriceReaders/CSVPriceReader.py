'''
Created on Apr 24, 2017

@author: stepan
'''
from PriceReader import PriceReader
import csv
from dydev.TradingSignals.PriceReaders.PriceInformation import PriceInformation

class CSVPriceReader(PriceReader) :

    def __init__(self, dbFile, skipFirstRow):
        self.csvreader = csv.reader(dbFile, delimiter=',')
        self.skipFirstRow = skipFirstRow

    def read(self, dateFormat, dateColIdx, openColIdx, highColIdx, lowColIdx, closeColIdx):
        rowIdx = -1
        for row in self.csvreader:
            rowIdx = rowIdx + 1
            if rowIdx == 0 and self.skipFirstRow:
                continue

            yield PriceInformation().fromCSV(row, \
                                             dateFormat=dateFormat,
                                             dateColIdx=dateColIdx,
                                             timeColIdx=None,
                                             openColIdx=openColIdx,
                                             highColIdx=highColIdx,
                                             lowColIdx=lowColIdx,
                                             closeColIdx=closeColIdx)
