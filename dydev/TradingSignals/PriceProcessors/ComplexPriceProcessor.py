'''
Created on Apr 24, 2017

@author: stepan
'''
from dydev.TradingSignals.PriceProcessors.PriceProcessor import PriceProcessor

class ComplexPriceProcessor(PriceProcessor):
    '''
    classdocs
    '''


    def __init__(self, priceProcessors):
        '''
        Constructor
        '''
        self.priceProcessors = priceProcessors
        
    def process(self, prices):
        self.lastProcessResult = [p.process(prices) for p in self.priceProcessors ]
                