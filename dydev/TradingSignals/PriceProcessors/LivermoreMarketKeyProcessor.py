'''
Created on Apr 24, 2017

@author: stepan
'''
import datetime
from dydev.TradingSignals.PriceProcessors.PriceProcessor import PriceProcessor
import itertools
from dydev.TradingSignals.PriceReaders.PriceInformation import PriceInformation
import sys

LIVERMORE_ATTR = 'livermore'
PRICE_BASE_ATTR = 'priceBase'
LAST_PRICE_ATTR = 'lastPrice'
LAST_LINE_PRICE_ATTR = 'lastLinePrice'
STOCK_NAME_ATTR = 'stockName'
KEY_PRICE_NAME = 'KEY'

ACTION_BUY = 'BUY'
ACTION_SELL = 'SELL'

COLUMN_THREND_UPWARD = 3
COLUMN_NATURAL_RALLY = 2
COLUMN_SECONDARY_RALLY = 1
COLUMN_THREND_DOWNWARD = -3
COLUMN_NATURAL_REACTION = -2
COLUMN_SECONDARY_REACTION = -1


COLUMN_THREND = 3
COLUMN_NATURAL = 2
COLUMN_SECONDARY = 1
COLUMN_IDLE = 0

COLUMNS = [\
    COLUMN_THREND_UPWARD, \
    COLUMN_NATURAL_RALLY, \
    COLUMN_SECONDARY_RALLY, \
    COLUMN_THREND_DOWNWARD, \
    COLUMN_NATURAL_REACTION, \
    COLUMN_SECONDARY_REACTION, \
]

COLUMN_TYPES = [\
    COLUMN_THREND_UPWARD, \
    COLUMN_NATURAL_RALLY, \
    COLUMN_SECONDARY_RALLY, \
]

PRICE_DIRECTION_DECREASING = -1
PRICE_DIRECTION_IDLE = 0
PRICE_DIRECTION_INCREASING = 1
PRICE_DIRECTIONS_WITH_IDLE = [\
    PRICE_DIRECTION_DECREASING,
    PRICE_DIRECTION_IDLE,
    PRICE_DIRECTION_INCREASING
]
PRICE_DIRECTIONS = [\
    PRICE_DIRECTION_DECREASING,
    PRICE_DIRECTION_INCREASING
]

PRICE_BASE_CONFIG_ATTR = "basePriceConfig"
BASE_TYPE_ATTR = "type"

BASE_PRICE_TYPE_FIXED = "fixed"
BASE_PRICE_TYPE_AVERAGE = "average"

BASE_PRICE_AVERAGE_PERIOD = "period"
BASE_PRICE_AVERAGE_FACTOR = "factor"

DATE_FORMAT = "%m/%d/%y"
TIME_FORMAT = "%H:%M.%S"


def getDatetime(v):
    return datetime.datetime.strptime(v, "%s %s" % (DATE_FORMAT, TIME_FORMAT))

def dirAttr(direction):
    if direction is PRICE_DIRECTION_DECREASING:
        return 'decr'
    if direction is PRICE_DIRECTION_INCREASING:
        return 'incr'
    if direction is PRICE_DIRECTION_IDLE:
        return 'idle'
    return 'none'

def dirFromAttr(attr):
    return PRICE_DIRECTION_DECREASING if attr is 'decr' else \
           PRICE_DIRECTION_INCREASING if attr is 'incr' else \
           PRICE_DIRECTION_IDLE if attr is 'idle' else \
           PRICE_DIRECTION_IDLE  # default

def colAttr(columnId):
    if columnId is COLUMN_IDLE:
        return 'idle'
    if columnId is COLUMN_THREND_UPWARD:
        return 'upthrend'
    if columnId is COLUMN_NATURAL_RALLY:
        return 'natrally'
    if columnId is COLUMN_SECONDARY_RALLY:
        return 'secrally'
    if columnId is COLUMN_THREND_DOWNWARD:
        return 'dnthrend'
    if columnId is COLUMN_NATURAL_REACTION:
        return 'natreact'
    if columnId is COLUMN_SECONDARY_REACTION:
        return 'secreact'
    return 'none'

def colFromAttr(attr):
    return COLUMN_THREND_UPWARD if attr is 'upthrend' else \
           COLUMN_NATURAL_RALLY if attr is 'natrally' else \
           COLUMN_SECONDARY_RALLY if attr is 'secrally' else \
           COLUMN_THREND_DOWNWARD if attr is 'dnthrend' else \
           COLUMN_NATURAL_REACTION if attr is 'natreact' else \
           COLUMN_SECONDARY_REACTION if attr is 'secreact' else \
           COLUMN_IDLE  # default

def directionalIncrement(direction, oldPrice, newPrice):
    if direction != PRICE_DIRECTION_IDLE:
        return (newPrice - oldPrice) * direction

    return (newPrice - oldPrice).abs()

def directionalMaximum(direction, oldPrice, newPrice):
    diff = directionalIncrement(direction, oldPrice, newPrice)
    if diff.getPoints() > 0:
        return newPrice
    return oldPrice

class LivermoreMarketSinglePriceProcessor(PriceProcessor):
    '''
    classdocs
    '''

    # in future we can use next operations:
    # getOppositeDirection(direction): return -direction
    #
    # getHigherImportanceColumn(columnIndex):
    #     return abs(columnIndex) > 3 ? columnIndex : \
    #         columnIndex > 0 ? columnIndex + 1 : columnIndex -1

    def __init__(self, livermorePreconditons, stockName, actionsHandler, \
                 outStream=None):
        '''
        Constructor
        '''
        self.out = outStream

        self.stockName = stockName
        self.dbHandler = actionsHandler
        self.lastPrice = None

        directions = [PRICE_DIRECTION_INCREASING, PRICE_DIRECTION_DECREASING]
        columns = [ \
                      COLUMN_THREND_UPWARD, \
                      COLUMN_NATURAL_RALLY, \
                      COLUMN_SECONDARY_RALLY ]

        self.columns = dict([(d * c, LivermorePriceColumn(d, c)) \
                             for (d, c) in itertools.product(directions, columns)])

        idleColumn = LivermorePriceColumn(PRICE_DIRECTION_IDLE, COLUMN_IDLE)

        self.columns[PRICE_DIRECTION_IDLE * COLUMN_IDLE] = idleColumn

        self.lastPriceColumn = idleColumn

        directions.append(PRICE_DIRECTION_IDLE)
        self.lastPriceColumnWithLine = dict([(d, None) for d in directions])
        self.lastPriceByDirection = dict([(d, None) for d in directions])
        self.lastPriceColumnByDirection = dict([(d, None) for d in directions])

        self.lastConfirmedThrendDirection = None

        if livermorePreconditons != None:
            self.loadPreconditions(livermorePreconditons)

    def loadPreconditions(self, historyInfo):
        COLUMNS_ATTR = 'columns'

        if COLUMNS_ATTR in historyInfo:
            for c in COLUMNS:
                col = colAttr(c)
                basePrice = \
                    float(historyInfo[PRICE_BASE_ATTR]) \
                    if PRICE_BASE_ATTR in historyInfo \
                    else 100.0

                if col in historyInfo[COLUMNS_ATTR]:
                    self.columns[c] = LivermorePriceColumn.fromJson(\
                        c, historyInfo[COLUMNS_ATTR][col], basePrice \
                    )

            self.calculateLastValues()

    def calculateLastValues(self):

        lineCols = dict(
            [ \
                (d, [ c \
                        for c in [ self.getThrendColumn(d), self.getNaturalColumn(d) ] \
                        if c.getLastLinePrice() \
                    ] \
                ) \
                for d in PRICE_DIRECTIONS \
            ] \
        )

        self.lastPriceColumnWithLine = dict([ \
            (d, \
                max(\
                    lineCols[d], \
                    key=lambda c: c.getLastLinePrice().getDatetime() \
                ) \
                if lineCols[d] \
                else None \
            ) \
            for d in PRICE_DIRECTIONS \
        ])

        directionCols = dict([\
            (d, \
                [ \
                    self.columns[c * d] for c in COLUMN_TYPES \
                    if self.columns[c * d].getLastPrice() \
                ] \
            ) \
            for d in PRICE_DIRECTIONS \
        ])

        self.lastPriceColumnByDirection = dict([ \
            (d, max(\
                    directionCols[d],
                    key=lambda c: c.getLastPrice().getDatetime()
                )
                if directionCols[d] \
                else None
            ) \
            for d in PRICE_DIRECTIONS \
        ])

        self.lastPriceByDirection = dict([ \
            (\
                d, self.lastPriceColumnByDirection[d].getLastPrice() \
                if self.lastPriceColumnByDirection[d] \
                else None \
            ) \
            for d in PRICE_DIRECTIONS \
        ])

        columnsWithLastPrice = [ c for c in self.columns.itervalues() if c.getLastPrice() ]

        if columnsWithLastPrice:
            self.lastPriceColumn = max(\
                columnsWithLastPrice, \
                key=lambda c: c.getLastPrice().getDatetime() \
            )

            if self.lastPriceColumn:
                self.lastPrice = self.lastPriceColumn.getLastPrice()

    def gotDirectionPrediction(self, direction):

        if self.isThrendConfirmed(direction):
            return True

        if self.gotDangerSignal(direction):
            return False

        if self.isThrendConfirmed(-direction):
            return False

        if self.gotDangerSignal(-direction):
            return True

        return self.lastConfirmedThrendDirection is direction

    def gotBuySignal(self):
        return self.gotDirectionPrediction(PRICE_DIRECTION_INCREASING)

    def gotSellSignal(self):
        return self.gotDirectionPrediction(PRICE_DIRECTION_INCREASING)

    # Implemented 10.c and 10.a rules
    def isThrendConfirmed(self, direction):
        thrend = self.getThrendColumn(direction)

        lastPrice = self.getLastPrice()

        if not lastPrice:
            return False

        lastPivotalPointPrice = thrend.getLastLinePrice()

        # if we are first time got thrend in this direction, so far return fasle
        if not lastPivotalPointPrice:
            return False

        if (lastPrice.getPoints() - thrend.getLastLinePrice().getPoints()) * direction > 3:
            self.lastConfirmedThrendDirection = direction
            if self.stockName != KEY_PRICE_NAME:
                self.dbHandler.registerThrendConfirmation(lastPrice.getId(), direction)
            return True

        return False

    # Implements 10.e and 10.f rules.
    def gotDangerSignal(self, direction):
        oppositeThrend = self.getThrendColumn(-direction)
        curDirection = self.getLastPriceColumn().getDirection()

        if not self.lastPrice or not oppositeThrend.getLastLinePrice():
            return False

        # If we were in oppositeThrend of checking direction, then we
        # are updating pivotal point right now, so, no danger signal.
        if self.getLastPriceColumn(direction).IsThrend():
            return False

        # Direction has not been changed. No danger so far.
        if curDirection is direction:
            return False

        lastPricePoints = self.lastPrice.getPoints()
        privotPointPricePoints = oppositeThrend.getLastLinePrice().getPoints()

        # Danger for currenct direction if:
        # We were on same direction ( but didn't cross last pivot point
        # and currently we are on opposite direction.
        dangerSignal = \
            (lastPricePoints - privotPointPricePoints) * direction < -3

        if dangerSignal:
            self.lastConfirmedThrendDirection = curDirection
            if self.stockName != KEY_PRICE_NAME:
                self.dbHandler.registerDangerSignal(self.lastPrice.getId(), direction)
            return True

        return False

    def process(self, price):

        self.dbHandler.addPriceSignal(price.getId())

        firstPass = self.lastPrice is None

        if firstPass:
            self.registerPrice(self.getIdleColumn(), price)
            for c in self.columns.itervalues():
                c.setupFirstPrice(price)
            return

        if price > self.lastPrice:
            self.processPrice(price, PRICE_DIRECTION_INCREASING)
        elif price < self.lastPrice:
            self.processPrice(price, PRICE_DIRECTION_DECREASING)
        else:
            self.ignorePrice(price, "no changes")


    # My own Livermore rule adaptations
    def processPriceStp(self, price, direction):
        lastPriceColumn = self.getLastPriceColumn()

        pointsDirectionalIncrement = \
            lastPriceColumn.directionalIncrement(price).getPoints()

        directionSwitched = False

        if pointsDirectionalIncrement < 0:
            if pointsDirectionalIncrement <= -6:
                directionSwitched = True
            else:
                self.ignorePrice(price, "direction changed, but less than 6 points")
                return

        # 6.e, 6.f and partially 6.g, 6.h rules
        self.tryMoveToMoreImportantColumnStp(price, direction, directionSwitched)

    def processPrice(self, price, direction, useModifiedLivermoreMethod=True):
        if useModifiedLivermoreMethod:
            self.processPriceStp(price, direction)
        else:
            self.processPriceOriginal(price, direction)

    def processPriceOriginal(self, price, direction):

        lastPriceColumn = self.getLastPriceColumn()

        pointsDirectionalIncrement = \
            lastPriceColumn.directionalIncrement(price).getPoints()

        # If we continue thrend direction, don't do anything special.
        if pointsDirectionalIncrement > 0 and \
           self.getLastPriceColumn().IsThrend():
            self.registerPrice(self.getLastPriceColumn(), price)
            return

        directionSwitched = False

        # process if price started to go in opposite direction
        # compare with same direction or idle
        # 6.a -- 6.d, partially 6.g, 6.h rules.
        # TODO: What if directional change less than 6 points?
        if pointsDirectionalIncrement < 0:
            if pointsDirectionalIncrement <= -5:
                directionSwitched = True
            else:
                self.ignorePrice(price, "direction changed, but less that 6 points")
                return

        # 6.e, 6.f and partially 6.g, 6.h rules
        self.tryMoveToMoreImportantColumn(price, direction, directionSwitched)

    # This is my own way how to switch column importance.
    # If we detected direction switch,
    # at first we register column by Livermore rules (secondary, reaction or
    # thrend)
    # But if then column extended 3 points more, then we increase column importance.
    # Also, no exceptions for secondary, always put lines, even for secondaries.
    def tryMoveToMoreImportantColumnStp(self, price, direction, directionSwitched=False):
        # directionSwitched = True
        # implements direction swithing cases, namely 6.a - 6.d rules.

        # lookup for best column type
        # modification of
        # 6.e and 6.f rules, transition from natural to thrend.
        newColumnType = COLUMN_SECONDARY
        for ct in [COLUMN_NATURAL, COLUMN_THREND]:
            if self.getColumnByType(direction, ct).continuesDirection(price):
                newColumnType = ct

        if newColumnType != COLUMN_THREND:
            # Now try to increase column importance
            lastPriceColumntWithLine = self.getLastPriceColumnWithLine(direction)

            if lastPriceColumntWithLine:
                changePrice = lastPriceColumntWithLine.getLastLinePrice()

                pointsDifference = \
                    directionalIncrement(\
                        direction, \
                        changePrice, \
                        price) \
                    .getPoints()

                if pointsDifference > 3:
                    newColumnType = self.incColumnImportance(newColumnType)

        self.registerPrice(self.getColumnByType(direction, newColumnType), price)

        if directionSwitched:
            lastPriceColumnOppositeDirection = self.getLastPriceColumn(-direction)
            self.registerLineIfNeeded(lastPriceColumnOppositeDirection, \
                                      ignoreSecondary=False)

    def tryMoveToMoreImportantColumn(self, price, direction, directionSwitched=False):

        # directionSwitched = True
        # implements direction swithing cases, namely 6.a - 6.d rules.

        # 4.a -- 4.d rules,
        # put lines in all 6 point price direction changes
        # except going into secondary columns.
        putLineIfNeeded = directionSwitched

        # 6.e and 6.f rules, transition from natural to thrend.
        if self.getThrendColumn(direction).continuesDirection(price):
            self.registerPrice(self.getThrendColumn(direction), price)
        else:

            naturalColumn = self.getNaturalColumn(direction)
            lastLinedPriceColumnSameDirection = self.getLastPriceColumnWithLine(direction)

            # 6.g and 6.h, second part, transition from secondary to natural
            # But register in natural always, if we switched to new direction
            # and last column with line for old direction was thrend.
            registerInNatural = \
                naturalColumn.continuesDirection(price) or \
                directionSwitched and (\
                    lastLinedPriceColumnSameDirection is None or \
                    lastLinedPriceColumnSameDirection.IsThrend() \
                )

            if registerInNatural:
                if not directionSwitched:
                    self.registerInNaturalOrThrend(price, direction)
                else:
                    self.registerPrice(naturalColumn, price)
            else:
                self.registerPrice(self.getSecondaryColumn(direction), price)
#                 # 4.a -- 4.d rules, exception for transition to secondary:
#                 # don't put lines if direction switched, but we went into
#                 # secondary column.
#                 putLineIfNeeded = False

        if directionSwitched and putLineIfNeeded:
            lastPriceColumnOppositeDirection = self.getLastPriceColumn(-direction)
            self.registerLineIfNeeded(lastPriceColumnOppositeDirection)

    # 5.a and 5.b, turning natural into thrend, due to lines crossing.
    def registerInNaturalOrThrend(self, price, direction):

        lastColumnWithLineOfThisDirection = self.getLastPriceColumnWithLine(direction)

        if lastColumnWithLineOfThisDirection != None and \
           lastColumnWithLineOfThisDirection.IsNatural():

            pointsDifference = \
                directionalIncrement(\
                    direction, \
                    self.getNaturalColumn(direction).lastLinePrice, \
                    price) \
                .getPoints()

            if pointsDifference > 3:
                self.registerPrice(self.getThrendColumn(direction), price)
            else:
                self.registerPrice(self.getNaturalColumn(direction), price)
        else:
            self.registerPrice(self.getNaturalColumn(direction), price)

    def ignorePrice(self, price, reason):
        self.lastPrice = price
        column = self.lastPriceColumn
        self.log("ignoreprice (%s): %s, p: %s, reason: %s" % (self.stockName, column.name(), price, reason))

        if self.stockName != KEY_PRICE_NAME:
            self.dbHandler.setColumn(price.getId(), column.getId())
            self.dbHandler.setPoints(price.getId(), price.getPoints())

    def registerPrice(self, column, price):
        self.log("regprice (%s):    %s, p: %s" % (self.stockName, column.name(), price))

        if self.stockName != KEY_PRICE_NAME:
            self.dbHandler.setColumn(price.getId(), column.getId())
            self.dbHandler.setPoints(price.getId(), price.getPoints())

        column.registerPrice(price)
        self.lastPriceColumn = column
        self.lastPrice = price
        self.lastPriceColumnByDirection[column.getDirection()] = column
        self.lastPriceByDirection[column.getDirection()] = price

    def registerLineIfNeeded(self, column, ignoreSecondary=True):

        # For my own modifications put lines also for secondary column.
        if ignoreSecondary and column.IsSecondary():
            return

        self.lastPriceColumnWithLine[column.getDirection()] = column
        price = column.getLastPrice()

        column.registerLine()

        self.log("regline (%s):     %s, p: %s" % (self.stockName, column.name(), column.getLastPrice()))
        priceId = price.getId()
        if self.stockName != KEY_PRICE_NAME:
            self.dbHandler.setLine(priceId)

    def getLastPrice(self, direction=None):
        if not direction:
            return self.lastPrice
        return self.lastPriceColumnByDirection[direction].getLastPrice()

    def getLastPriceColumn(self, direction=None):
        if not direction:
            return self.lastPriceColumn
        return self.lastPriceColumnByDirection[direction]

    def getLastDirection(self):
        return self.lastPriceColumn.getDirection()

    def getLastPriceColumnWithLine(self, direction):
        return self.lastPriceColumnWithLine[direction]

    def getIdleColumn(self):
        return self.columns[PRICE_DIRECTION_IDLE * COLUMN_IDLE]

    def getThrendColumn(self, direction):
        return self.columns[direction * COLUMN_THREND_UPWARD]

    def getNaturalColumn(self, direction):
        return self.columns[direction * COLUMN_NATURAL_RALLY]

    def getSecondaryColumn(self, direction):
        return self.columns[direction * COLUMN_SECONDARY_RALLY]

    def getColumnByType(self, direction, columnType):
        return self.columns[direction * columnType]

    @staticmethod
    def incColumnImportance(columnType):
        if columnType < COLUMN_THREND:
            return columnType + 1
        else:
            return columnType

    @staticmethod
    def decColumnImportance(columnType):
        if columnType > COLUMN_SECONDARY:
            return columnType - 1
        else:
            return COLUMN_SECONDARY

    def log(self, s):
        if self.out:
            print >> self.out, s


class LivermorePriceColumn(object):

    def __init__(self, direction, columnType, lastPrice=None, lastLinePrice=None):
        self.direction = direction
        self.columnType = columnType
        self.lastPrice = lastPrice
        self.lastLinePrice = lastLinePrice

    @staticmethod
    def fromJson(columnId, columnJson, basePrice):
        lastPrice = LivermorePrice.fromPriceInformation(\
                        PriceInformation().fromCSV(columnJson[LAST_PRICE_ATTR]), \
                        basePrice
                    )

        lastLinePrice = LivermorePrice.fromPriceInformation(\
            PriceInformation.fromCSV(columnJson[LAST_LINE_PRICE_ATTR]), \
            basePrice \
        ) \
        if LAST_LINE_PRICE_ATTR in columnJson \
        else \
        None

        direction = PRICE_DIRECTION_DECREASING if columnId < 0 else \
                    PRICE_DIRECTION_INCREASING if columnId > 0 else \
                    PRICE_DIRECTION_IDLE

        return LivermorePriceColumn(direction, abs(columnId), lastPrice, lastLinePrice)

    def getDirection(self):
        return self.direction

    def getLastPrice(self):
        return self.lastPrice

    def getLastLinePrice(self):
        return self.lastLinePrice

    def getColumnType(self):
        return self.columnType

    def IsIdle(self):
        return self.columnType == COLUMN_IDLE

    def IsThrend(self):
        return abs(self.columnType) == COLUMN_THREND_UPWARD

    def IsNatural(self):
        return abs(self.columnType) == COLUMN_NATURAL_RALLY

    def IsSecondary(self):
        return abs(self.columnType) == COLUMN_SECONDARY_RALLY

    def continuesDirection(self, price):
        if not self.lastPrice:
            return True

        dirMax = self.lastPrice if self.lastLinePrice is None else \
            directionalMaximum(self.direction, self.lastPrice, self.lastLinePrice)

        return (price - dirMax) * self.direction > 0

    def directionalIncrement(self, price):
        return directionalIncrement(self.direction, self.lastPrice, price)

    def setupFirstPrice(self, price):
        self.lastPrice = price

    def registerPrice(self, price):
        self.lastPrice = price

    def registerLine(self):
        self.lastLinePrice = self.lastPrice

    def name(self):
        id = self.getId()
        return "%s(%s)" % (str(id).rjust(2), colAttr(id))

    def getId(self):
        return self.columnType * self.direction

    def __str__(self):
        return "%s, lastPrice: %s, lastLine: %s" % (self.columnType * self.direction, self.lastPrice, self.lastLinePrice)

class LivermorePrice(object):

    def __init__(self, \
                 id=None,
                 priceValue=0.0, \
                 priceDatetime=datetime.datetime(1900, 1, 1), \
                 pricePointsBase=100):

        self.id = id
        self.value = priceValue
        self.datetime = priceDatetime
        self.pointsBase = pricePointsBase

    @staticmethod
    def fromPriceInformation(rawPrice, baseValue):
        return LivermorePrice(rawPrice.id, rawPrice.closePrice, rawPrice.datetime, baseValue)

    def getId(self):
        return self.id

    def getPoints(self):
        return (self.value / self.pointsBase) * 100

    def getBase(self):
        return self.pointsBase

    def getValue(self):
        return self.value

    def getDatetime(self):
        return self.datetime

    def setPrice(self, value, base=None, priceDatetime=None):
        self.value = value
        if base:
            self.pointsBase = base

        if priceDatetime:
            self.datetime = priceDatetime

    def abs(self):
        return LivermorePrice(id=None, \
                              priceValue=abs(self.value), \
                              priceDatetime=self.datetime, \
                              pricePointsBase=self.pointsBase)

    def __str__(self):
        return "%s, value %s, base %s" % (self.datetime, self.value, self.pointsBase)

    def isLaterThan(self, other):

        if self.date > other.date:
            return True

        if self.date == other.date:
            return self.time > other.time

        return False

    def __eq__(self, other):
        if (type(other) is LivermorePrice):
            return self.value == other.value
        return self.value == other

    def __ne__(self, other):
        if (type(other) is LivermorePrice):
            return self.value != other.value
        return self.value != other

    def __gt__(self, other):
        if (type(other) is LivermorePrice):
            return self.value > other.value
        return self.value > other

    def __lt__(self, other):
        if (type(other) is LivermorePrice):
            return self.value < other.value
        return self.value < other

    def __add__(self, other):
        if (type(other) is LivermorePrice):
            return LivermorePrice(None, self.value + other.value, max(self.datetime, other.datetime), self.pointsBase)
        return LivermorePrice(None, self.value + other, self.datetime, self.pointsBase)

    def __sub__(self, other):
        if (type(other) is LivermorePrice):
            return LivermorePrice(None, self.value - other.value, max(self.datetime, other.datetime), self.pointsBase)
        return LivermorePrice(None, self.value - other, self.datetime, self.pointsBase)

    def __mul__(self, other):
        return LivermorePrice(None, self.value * other, self.datetime, self.pointsBase)

class LivermoreMarketKeyProcessor(PriceProcessor):
    '''
    classdocs
    TODO: Now we need sinlge processors interaction.
    If we perform sell or buy operations, we should reset column lines.
    And thus we should exclude transition to secondary.
    '''

    def __init__(self, config, dbHandler, tradingHandler=None):
        '''
        Constructor
        '''

        self.dbHandler = dbHandler

        self.tradingHandler = tradingHandler

        self.startDatetime = getDatetime(config['start']) if 'start' in config else None
        self.endDatetime = getDatetime(config['end']) if 'end' in config else None

        historyInfos = config['historyInfos']

        self.stocksSet = [h[STOCK_NAME_ATTR] for h in historyInfos if h[STOCK_NAME_ATTR] != KEY_PRICE_NAME]
        self.dbHandler.initSession("Livermore, %s" % ','.join(self.stocksSet))


        self.configBasePriceBehaviour(config)

        baseConfig = config[PRICE_BASE_CONFIG_ATTR] \
            if PRICE_BASE_CONFIG_ATTR in config else None

        self.priceProcessors = \
        dict(\
            [ \
                (\
                    h[STOCK_NAME_ATTR], \
                    LivermoreMarketSinglePriceProcessor(\
                        h[LIVERMORE_ATTR], h[STOCK_NAME_ATTR], \
                        self.dbHandler, sys.stdout) \
                ) \
                if LIVERMORE_ATTR in h else \
                LivermoreMarketSinglePriceProcessor() \
                for h in historyInfos \
            ] \
        )

        self.loadPriceBaseValues(historyInfos)

        self.lastSignalDirection = PRICE_DIRECTION_IDLE
        self.lastSignalPrice = LivermorePrice()
        self.actionThresholdInPoints = 12;

    def configBasePriceBehaviour(self, config):

        self.basePriceType = BASE_PRICE_TYPE_FIXED

        if not (PRICE_BASE_CONFIG_ATTR in config):
            return

        baseConfig = config[PRICE_BASE_CONFIG_ATTR]

        if BASE_TYPE_ATTR in baseConfig:
            self.basePriceType = str(baseConfig[BASE_TYPE_ATTR])

        if self.basePriceType == BASE_PRICE_TYPE_AVERAGE:
            self.basePricePeriod = float(baseConfig[BASE_PRICE_AVERAGE_PERIOD])
            self.basePriceAverageFactor = float(baseConfig[BASE_PRICE_AVERAGE_FACTOR]) \
                if BASE_PRICE_AVERAGE_FACTOR in baseConfig else 1

    def loadPriceBaseValues(self, historyInfos):
        self.priceBaseValues = \
        dict(\
            [ \
                (\
                    h[STOCK_NAME_ATTR],
                    float(h[LIVERMORE_ATTR][PRICE_BASE_ATTR]) \
                ) \
                if LIVERMORE_ATTR in h and \
                   PRICE_BASE_ATTR in h[LIVERMORE_ATTR] \
                else 100 \
                for h in historyInfos \
            ] \
        )

    def process(self, pricesSource):

        minDatetime = self.startDatetime if self.startDatetime else self.dbHandler.findMinDatetime()
        maxDatetime = self.endDatetime if self.endDatetime else self.dbHandler.findMaxDatetime()

        curDate = minDatetime

        while curDate < maxDatetime:
            prices = self.getPrices(curDate)
            if prices:
                self.processPrices(prices)
            curDate += datetime.timedelta(days=1)

    def getPrices(self, curDate):
        rawPrices = self.dbHandler.getPricesForDate(curDate, self.stocksSet)
        prices = dict(\
            [ \
                (s, LivermorePrice.fromPriceInformation(p, self.priceBaseValues[s])) \
                for (s, p) in rawPrices.iteritems()
            ] \
        )
        # Add KEY price
        keyPriceValue = sum([p.getPoints() for p in prices.itervalues()], 0)
        keyPrice = LivermorePrice(id=None, \
                                  priceValue=keyPriceValue, \
                                  priceDatetime=curDate, \
                                  pricePointsBase=self.priceBaseValues[KEY_PRICE_NAME])
        prices[KEY_PRICE_NAME] = keyPrice
        return prices

    def processPrices(self, prices):

        self.updateBaseValuesIfNeeded(prices)

        # Analize each stock separate
        for (s, p) in prices.iteritems():
            self.priceProcessors[s].process(p)

        if not self.handleThrendPrediction(PRICE_DIRECTION_INCREASING, prices, \
                                           ACTION_BUY):

            self.handleThrendPrediction(PRICE_DIRECTION_DECREASING, prices, \
                                        ACTION_SELL)

    def updateBaseValuesIfNeeded(self, prices):
        if self.basePriceType == BASE_PRICE_TYPE_AVERAGE:
            period = self.basePricePeriod
            factor = self.basePriceAverageFactor
            for (s, p) in prices.iteritems():
                if s == KEY_PRICE_NAME:
                    continue
                newBase = factor * (p.getValue() + self.priceBaseValues[s] * (period - 1) / factor) / period
                self.priceBaseValues[s] = newBase
                print " %s: base is %s" % (s, newBase)

    def handleThrendPrediction(self, direction, prices, signalName):

        newKeyPrice = prices[KEY_PRICE_NAME]

        # We need to run this method for each stock
        # So don't pass it to 'all' directly.
        gotPredictions = [p.gotDirectionPrediction(direction) for p in self.priceProcessors.itervalues()]

        if all(gotPredictions) and \
            (self.lastSignalDirection != direction or \
             (newKeyPrice - self.lastSignalPrice).getPoints() * direction > self.actionThresholdInPoints):
            print "Got %s signal." % signalName
            self.lastSignalDirection = direction
            self.lastSignalPrice = newKeyPrice
            self.dbHandler.setAction([p.getId() for (s, p) in prices.iteritems() if s != KEY_PRICE_NAME ], signalName)
            if self.tradingHandler:
                self.tradingHandler.handleAction(prices, signalName)
