'''
Created on May 26, 2017

@author: stepan
'''

import sqlite3
from contextlib import contextmanager
from multiprocessing import connection
from dydev.IOUtils.OsUtils import existsNotEmpty, openFiles
from dydev.TradingSignals.PriceProcessors.LivermoreMarketKeyProcessor import LivermoreMarketKeyProcessor, \
    LivermorePrice
from dydev.TradingSignals.PriceProcessors.ComplexPriceProcessor import ComplexPriceProcessor
from dydev.TradingSignals.PriceReaders.CSVPriceReader import CSVPriceReader
from dydev.TradingSignals.PriceReaders.PriceInformation import PriceInformation
from datetime import datetime, timedelta

DB_ATTR = "database"
PATH_ATTR = "path"
VALUES_FILE_ATTR = "valuesFile"
STOCK_NAME_ATTR = "stockName"
PRICE_BASE_ATTR = 'priceBase'
LIVERMORE_ATTR = 'livermore'

HISTORY_INFOS_ATTR = 'historyInfos'

SKIP_FIRST_ROW_ATTR = "skipFirstRow"

DATE_FORMAT_ATTR = "dateFormat"
DATE_COLUMN_ATTR = "dateColumn"
OPEN_COLUMN_ATTR = "openColumn"
HIGH_COLUMN_ATTR = "highColumn"
LOW_COLUMN_ATTR = "lowColumn"
CLOSE_COLUMN_ATTR = "closeColumn"

DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"
DATETIME_FORMAT = "%s %s" % (DATE_FORMAT, TIME_FORMAT)

class SQLPricesSource(object):
    def __init__(self, jsonConfig=None, dbFile=None):
        self.sessionId = None

        if jsonConfig:
            self.dbPath = jsonConfig[DB_ATTR][PATH_ATTR]
        if dbFile:
            self.dbPath = dbFile

        with self.dbConnect() as connection:
            connection.createTables()

    def initSession(self, processorName):
        with self.dbConnect() as connection:
            self.sessionId = connection.initSession(processorName)

    def addPriceSignal(self, priceId):
        with self.dbConnect() as connection:
            connection.addPriceSignal(priceId)

    def setColumn(self, priceId, columnId):
        with self.dbConnect() as connection:
            connection.setColumn(priceId, columnId)

    def setPoints(self, priceId, baseValue):
        with self.dbConnect() as connection:
            connection.setPoints(priceId, baseValue)

    def setLine(self, priceId):
        with self.dbConnect() as connection:
            connection.setLine(priceId)

    def setAction(self, priceIds, actionMark):
        with self.dbConnect() as connection:
            connection.setAction(priceIds, actionMark)

    def registerThrendConfirmation(self, priceId, direction):
        with self.dbConnect() as connection:
            connection.registerThrendConfirmation(priceId, direction)

    def registerDangerSignal(self, priceId, direction):
        with self.dbConnect() as connection:
            connection.registerDangerSignal(priceId, direction)

    def dumpPricesTable(self, out):
        with self.dbConnect() as connection:
            for p in connection.getPrices():
                print >> out, p

    def findMinDatetime(self):
        with self.dbConnect() as connection:
            return connection.findMinDatetime()

    def findMaxDatetime(self):
        with self.dbConnect() as connection:
            return connection.findMaxDatetime()

    def readDates(self):
        minDatetime = self.findMinDatetime()
        maxDatetime = self.findMaxDatetime()

        curDate = minDatetime

        while curDate <= maxDatetime:
            yield curDate
            curDate += timedelta(days=1)

    @staticmethod
    def importPricesFromFile(database, fileName, stockName, importConfig):
        # Create table is needed:
        sqlSource = SQLPricesSource(dbFile=database)

        return sqlSource.importPricesFromFileInternal(fileName, \
                                                      stockName, \
                                                      importConfig)

    def importPricesFromFileInternal(self, fileName, stockName, importConfig):

        dateFormat = importConfig[DATE_FORMAT_ATTR]
        dateColIdx = importConfig[DATE_COLUMN_ATTR]
        openColIdx = importConfig[OPEN_COLUMN_ATTR]
        highColIdx = importConfig[HIGH_COLUMN_ATTR]
        lowColIdx = importConfig[LOW_COLUMN_ATTR]
        closeColIdx = importConfig[CLOSE_COLUMN_ATTR]
        skipFirstRow = importConfig[SKIP_FIRST_ROW_ATTR] == 1
        with open(fileName) as f:
            with self.dbConnect() as connection:
                priceReader = CSVPriceReader(f, skipFirstRow=skipFirstRow)
                for p in priceReader.read(
                    dateFormat=dateFormat,
                    dateColIdx=dateColIdx, \
                    openColIdx=openColIdx, \
                    highColIdx=highColIdx, \
                    lowColIdx=lowColIdx, \
                    closeColIdx=closeColIdx, \
                    ):
                    connection.addPrice(stockName, p)

        return 0

    def importPrices(self, config, calcKeyPrice=True):

        # MAIN BODY #

        historyInfos = [historyInfo for historyInfo in config[HISTORY_INFOS_ATTR] ]
        fileNames = [h[VALUES_FILE_ATTR] for h in historyInfos if VALUES_FILE_ATTR in h]

        (res, wrongFile) = existsNotEmpty(fileNames, reportNonExisting=True)

        if not res:
            print ("File %s doesn't exist or empty." % wrongFile)
            return 1

        for fDesc in historyInfos:
            if not VALUES_FILE_ATTR in fDesc:
                continue

            fileName = fDesc[VALUES_FILE_ATTR]
            stockName = fDesc[STOCK_NAME_ATTR]

            with open(fileName) as f:
                with self.dbConnect() as connection:
                    priceReader = CSVPriceReader(f)
                    for p in priceReader.read():
                        connection.addPrice(stockName, p)

        for curDate in self.readDates():
            prices = self.getPricesForDate(curDate)
            if not prices:
                continue
#             keyPrice = sum([p for (s, p) in prices.iteritems() if s != "KEY"], PriceInformation())
#             with self.dbConnect() as connection:
#                 connection.addPrice("KEY", keyPrice)

    def addPrice(self, stockName, price):
        with self.dbConnect() as connection:
            return connection.addPrice(stockName, price)

    def getPricesForDate(self, datetime, stocksSet):
        with self.dbConnect() as connection:
            return connection.getPricesForDate(datetime, stocksSet)

    @contextmanager
    def dbConnect(self):
        try:
            connection = sqlite3.connect(self.dbPath)
            yield SQLLiteConnection(connection, self.sessionId)
        finally:
            connection.commit()
            connection.close()

class SQLLiteConnection(object):
    def __init__(self, connection, sessionId=None):
        self.connection = connection
        self.sessionId = sessionId

    def createTables(self):
        c = self.connection.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS PriceTable (
                         id integer primary key autoincrement,
                         stockName text,
                         valueDate text,
                         valueTime text,                         
                         maxValue real,
                         minValue real,
                         openValue real,
                         closeValue real,
                         UNIQUE (stockName, valueDate, valueTime) ON CONFLICT IGNORE
                    )
                    ''')

        c.execute('''CREATE TABLE IF NOT EXISTS PriceSignals (
                         id integer primary key autoincrement,
                         sessionId integer,
                         priceId integer,
                         pricePoints real,
                         column integer,
                         lineMark integer nullable,
                         threndConfirmation integer,
                         dangerSignal integer,
                         actionMark text nullable,
                         FOREIGN KEY (priceId) REFERENCES PriceTable(id),
                         FOREIGN KEY (sessionId) REFERENCES ProcessingSession(id)
                    )''')

        c.execute('''CREATE TABLE IF NOT EXISTS ProcessingSession (
                         id integer primary key autoincrement,
                         timestamp text,
                         processorName text
                    )''')

        self.sessionId = None

        pass

    def initSession(self, processorName):
        # Insert a row of data
        c = self.connection.cursor()

        sessionId = c.execute('''
            INSERT INTO ProcessingSession(timestamp, processorName)
            VALUES (?, ?)
            ''', (datetime.now(), processorName) \
        ).lastrowid

        return sessionId

    def addPriceSignal(self, priceId):
        c = self.connection.cursor()

        signalId = c.execute('''
            INSERT INTO PriceSignals(priceId, sessionId)
            VALUES (?, ?)
            ''', (priceId, self.sessionId) \
        ).lastrowid

        return signalId

    def getPriceSignal(self, priceId):

        c = self.connection.cursor()
        rows = [r for r in c.execute('''
                SELECT p.id FROM PriceSignals as p
                WHERE p.priceId = ? and p.sessionId = ?''', (priceId, self.sessionId))]

        return rows[0][0]

    def getPriceSignals(self, priceIds):

        c = self.connection.cursor()

        query = '''
            SELECT p.id from PriceSignals as p
            WHERE priceId in (%s) and p.sessionId = %s''' % \
            (",".join(str(i) for i in priceIds), self.sessionId)

        signalIds = [r[0] for r in c.execute(query)]
        return signalIds

    def addPrice(self, stockName, price):
        # Insert a row of data
        c = self.connection.cursor()
        id = c.execute('''
            INSERT OR IGNORE INTO PriceTable(stockName, valueDate, valueTime, maxValue, minValue, openValue, closeValue)
            VALUES (?,?,?,?,?,?,?)
            ''', (stockName, \
               datetime.strftime(price.getDatetime(), DATE_FORMAT), \
               datetime.strftime(price.getDatetime(), TIME_FORMAT), \
               price.getMax(), price.getMin(), \
               price.getOpen(), price.getClose() \
            ) \
        ).lastrowid
        return id

    def setColumn(self, priceId, columnId):

        priceSignalId = self.getPriceSignal(priceId)

        c = self.connection.cursor()

        c.execute('''
            UPDATE PriceSignals SET
            column = ?
            WHERE id = ?''', (columnId, priceSignalId) \
        )

    def setPoints(self, priceId, points):

        priceSignalId = self.getPriceSignal(priceId)

        c = self.connection.cursor()

        c.execute('''
            UPDATE PriceSignals SET
            pricePoints = ?
            WHERE id = ?''', (points, priceSignalId) \
        )

    def setLine(self, priceId):

        priceSignalId = self.getPriceSignal(priceId)

        c = self.connection.cursor()
        c.execute('''
            UPDATE PriceSignals SET
            lineMark = ?
            WHERE id = ?''', (1, priceSignalId) \
        )

    def setAction(self, priceIds, actionMark):

        priceSignalIds = self.getPriceSignals(priceIds)

        c = self.connection.cursor()

        query = '''
            UPDATE PriceSignals SET
            actionMark = '%s'
            WHERE id in (%s)''' % \
            (actionMark, ",".join(str(i) for i in priceSignalIds))

        c.execute(query)

    def registerThrendConfirmation(self, priceId, direction):

        priceSignalId = self.getPriceSignal(priceId)

        c = self.connection.cursor()

        c.execute('''
            UPDATE PriceSignals SET
            threndConfirmation = ?
            WHERE id = ?''',
            (direction, priceSignalId))

    def registerDangerSignal(self, priceId, direction):

        priceSignalId = self.getPriceSignal(priceId)

        c = self.connection.cursor()

        c.execute('''
            UPDATE PriceSignals SET
            dangerSignal = ?
            WHERE id = ?''',
            (direction, priceSignalId))


    def getPrices(self):
        c = self.connection.cursor()
        prices = c.execute('''SELECT * FROM PriceTable''')
        for p in prices:
            yield p

    def findMinDatetime(self):
        c = self.connection.cursor()
        row = [r for r in c.execute('''SELECT MIN(datetime(p.valueDate, p.valueTime)) FROM PriceTable as p''')][0][0]
        row = datetime.strptime(row, DATETIME_FORMAT)
        return row

    def findMaxDatetime(self):
        c = self.connection.cursor()
        row = [r for r in c.execute('''SELECT MAX(datetime(p.valueDate, p.valueTime)) FROM PriceTable as p''')][0][0]
        row = datetime.strptime(row, DATETIME_FORMAT)
        return row


    def getPricesForDate(self, valueDatetime, stocksSet):
        c = self.connection.cursor()
        strDate = datetime.strftime(valueDatetime, DATE_FORMAT)

        query = '''
            SELECT * FROM PriceTable as p
            WHERE p.valueDate = ? and p.stockName in (%s)''' % \
            (",".join("'%s'" % str(i) for i in stocksSet))

        prices = dict([ (p[1], PriceInformation().fromDb(p, DATE_FORMAT, TIME_FORMAT)) for p in c.execute(query, (strDate,))])
        return prices
